module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',

  },
  extends: [
    'airbnb-base',

  ],
  // required to lint *.vue files
  plugins: [
    'html',
  ],
  // add your custom rules here
  rules: {
    'import/no-extraneous-dependencies': ['warn'],
    'import/extensions': ['warn'],
    'import/no-unresolved': ['warn'],
    'no-param-reassign': ["error", { "props": false }],
  },
};

import io from 'socket.io-client';
import Message from './message';
import messageSound from './sound/new_message.mp3';
import MessageStore from './messageStore';

const audio = new Audio();
audio.src = messageSound;

class Client {
  constructor(userId) {
    this.socket = null;
    this.userId = +userId;
    this.recipientId = null;
    this.chatId = null;
    this.countEventTyping = 0;
    this.accesTypingEvent = true;
    this.chatObserver = null;
    this.messagesLimit = 70;
    this.scrollHistoryMessages = true;
    this.messages = null;
    this.lastSentId = 0;

    this.messageArea = document.getElementById('messages-area');
    this.messageTemplate = document.getElementById('message-template');
    document.getElementById('sender-id').innerText = this.userId;
    this.typingBlock = document.querySelector('.chat__messages').querySelector('.typing');
  }

  connect() {
    // this.chatId = this.makeChatId();
    this.socket = io('http://localhost:2121', {
      path: '/',
      transports: ['websocket', 'polling'],
      query: {
        // chatId: this.chatId,
        token: this.userId,
        // recipient: document.getElementById('uid_from').value,
      },
    });
    this.drowStatus('Подключение...', 'grey');

    // События
    this.socket.on('connect', () => this.drowStatus('Подключено', 'green'));
    this.socket.on('connect_error', () => this.drowStatus('Ошибка подключения', 'red'));
    this.socket.on('connect_timeout', () => this.drowStatus('connect_timeout...', 'red'));
    this.socket.on('error', () => this.drowStatus('Ошибка', 'red'));
    this.socket.on('disconnect', () => this.drowStatus('Сервер разорвал соеденение', 'red'));
    this.socket.on('reconnect', () => this.drowStatus('Переподключение...', 'yellow'));
    this.socket.on('reconnect_attempt', () => this.drowStatus('Попытка переподключения', 'yellow'));
    this.socket.on('reconnecting', () => this.drowStatus('Переподключение...', 'yellow'));
    this.socket.on('reconnect_error', () => this.drowStatus('Ошибка переподключения', 'red'));
    this.socket.on('reconnect_failed', () => this.drowStatus('reconnect_failed...', 'yellow'));

    this.socket.on('ping', () => console.log('ping'));
    this.socket.on('pong', latency => console.log(`pong: ${latency}`));

    // Сокет события
    this.socket.on('messages:new', data => this.newMessageHandler(data));
    this.socket.on('messages:readed', data => this.onReaded(data));
    this.socket.on('messages:typing', data => this.onCompanionTyping(data));
  }

  /**
   * Запуск диалога
   */
  start() {
    this.changeRecipient();
    this.messages = new MessageStore(this.userId);
    this.observers();

    (async () => {
      await this.getHistory();
      this.resendingNotSentMessages();
    })();
  }

  /**
   * Получаем собеседника
   */
  changeRecipient() {
    this.recipientId = document.getElementById('recipient-id').value;
    this.messageArea.innerHTML = '';
    this.chatId = this.getChatId();

    document.getElementById('messages-send-button').disabled = false;
  }

  /**
   * Назначение наблюдателей за чатом
   */
  observers() {
    // Открытие закрытие окна
    document.addEventListener('visibilitychange', () => {
      if (!document.hidden) {
        this.searchUnreadMessage();
      }
    }, false);

    // Добавление нового дочернего узла в окно чата "нового сообщения"
    this.chatObserver = new MutationObserver(() => {
      if (!document.hidden) {
        this.searchUnreadMessage();
      }
    });
    this.chatObserver.observe(this.messageArea, { childList: true });
  }

  /**
   * Рендеринг истории сообщений
   */
  async getHistory() {
    const offset = Object.keys(this.messages.all).length;
    const limit = this.messagesLimit;
    const startMessageId = this.getStartMessageId();

    await new Promise((resolve) => {
      this.socket.emit(
        'messages:getLastMessages',
        {
          recipient: this.recipientId,
          offset,
          limit,
          startMessageId,
        },
        (data) => {
          this.checkHistory(data.length);
          data.forEach((item) => {
            this.messages.all[item.id] = new Message(this.userId, item);
            this.messages.renderMessage[item.id] = this.messages.all[item.id];
            this.messages.isUnread(this.messages.all[item.id]);
          });
          this.messages.render();
          resolve();
        },
      );
    });
  }


  newMessageHandler(data) {
    const { message, tempId } = data;

    if (this.chatId == message.chat_id) {
      this.onNewMessage(message, tempId);
    } else if (this.userId == message.recipient && this.recipientId != message.sender) {
      iziToast.show({
        message: `Вам новое сообщение от пользователя ${message.sender}`,
        color: 'blue',
      });
      this.soundNewMessage(message);
    } else if (message.error) {
      console.log(message.error);
    }
  }


  /**
   * Рендеринг нового сообщения
   * @param message
   */
  onNewMessage(message) {
    if (!this.messages.checkDeliveredMessage(message.id)) {
      this.messages.all[message.id] = new Message(this.userId, message);
      this.messages.all[message.id].renderNewMessage(this.messageArea);
      this.messages.isUnread(this.messages.all[message.id]);
      this.typingBlock.innerText = '';
      this.soundNewMessage(message);
    }
  }


  /**
   * Изменяет статус сообщения на "Доставлено"
   * @param tempId
   * @param messageId
   */
  messageDelivered(tempId, messageId) {
    if (this.messages.checkSentMessage(tempId)) {
      this.messages.delivered[messageId] = this.messages.sent[tempId];
      delete this.messages.sent[tempId];
      this.messages.delivered[messageId].markAsDelivered();
      this.messages.delivered[messageId].changeId(messageId);
      this.messages.all[messageId] = this.messages.delivered[messageId];
      this.deleteLocalStorageMessage(tempId);
    }
  }

  /**
   * Подготовка отправки сообщения
   */
  prepareSend() {
    const message = document.getElementById('new-message').value;
    document.getElementById('new-message').value = '';
    this.prepareMessage(message);
    this.send();
  }

  /**
   * Обработка сообщения перед отправкой
   */
  prepareMessage(message) {
    if (message.length > 1024) {
      const countSend = Math.ceil(message.length / 1024);
      for (let i = 0; i < countSend; i += 1) {
        const startMessage = i * 1024;
        const lengthMessage = startMessage + 1024;
        const splitMessage = message.substring(startMessage, lengthMessage);
        this.renderSentMessage(splitMessage);
      }
    } else if (message.length === 0) {
      iziToast.show({
        message: 'Введите сообщение',
        color: 'red',
      });
    } else {
      this.renderSentMessage(message);
    }
  }


  /**
   * Рендер отправлающегося сообщения
   * @param message
   */
  renderSentMessage(message) {
    const messageObj = this.createMessage(message);
    this.messages.readyToSent[messageObj.id] = new Message(this.userId, messageObj);
    this.messages.readyToSent[messageObj.id].renderNewMessage(this.messageArea);
    this.setLastSentId(messageObj.id);
  }

  /**
   * Создание Обьекта сообщения
   * @param message
   * @return object
   */
  createMessage(message) {
    const messageObj = {
      chat_id: this.chatId,
      id: this.tempId(),
      message,
      readed: false,
      recipient: this.recipientId,
      sended_at: null,
      sender: this.userId,
      delivered: false,
    };
    return messageObj;
  }


  /**
   * Отправка сообщения
   */
  async send() {
    const idMessages = Object.keys(this.messages.readyToSent);

    this.notSentMessages();

    for (let i = 0; i < idMessages.length; i += 1) {
      const idMessage = idMessages[i];
      const messageObj = this.messages.getReadyToSent(idMessage);

      await new Promise((resolve) => {
        this.socket.emit(
          'messages:send',
          {
            recipient: this.recipientId,
            message: messageObj.message,
            tempId: messageObj.id,
            sended_at: messageObj.sended_at,
          },

          (response) => {
            if (response.error) {
              iziToast.show({
                message: `Ошибка отправки сообщения: ${response.error}`,
                color: 'red',
              });
            } else {
              resolve(this.messageDelivered(response.tempId, response.messageId));
            }
          },
        );
      });
    }
  }


  /**
   * Проверка localStorage на наличие неотправленных сообщений
   * Повторная отправка при наличии сообщений
   */
  resendingNotSentMessages() {
    if (window.localStorage.getItem('notSentMessages') != null) {
      let notSentMessages = {};
      notSentMessages = JSON.parse(window.localStorage.getItem('notSentMessages'));
      Object.keys(notSentMessages).forEach((chatId) => {
        if (chatId == this.chatId) {
          Object.keys(notSentMessages[chatId]).forEach((messageId) => {
            this.prepareMessage(notSentMessages[chatId][messageId]);
          });
        }
      });
      this.send();
    }
  }

  /**
   * Запись неоптравленных сообщений в localStorage
   */
  notSentMessages() {
    let haveChat = false;
    const notSendMessages = {};
    const chatIdMessages = {};

    Object.keys(this.messages.readyToSent).forEach((item) => {
      notSendMessages[item] = this.messages.readyToSent[item].message;
    });
    Object.keys(this.messages.sent).forEach((item) => {
      notSendMessages[item] = this.messages.sent[item].message;
    });

    if (Object.keys(notSendMessages).length > 0) {
      chatIdMessages[this.chatId] = notSendMessages;

      let localStorage = null;

      if (window.localStorage.getItem('notSentMessages') != null) {
        localStorage = JSON.parse(window.localStorage.getItem('notSentMessages'));
        Object.keys(localStorage).forEach((chatId) => {
          if (chatId == this.chatId) {
            haveChat = true;
            localStorage[chatId] = notSendMessages;
          }
        });
        if (!haveChat) {
          localStorage[this.chatId] = notSendMessages;
        }
        window.localStorage.setItem('notSentMessages', JSON.stringify(localStorage));
      } else {
        window.localStorage.setItem('notSentMessages', JSON.stringify(chatIdMessages));
      }
    }
  }

  /**
   * удаление сообщения из localStorage
   * @param tempId
   */
  deleteLocalStorageMessage(tempId) {
    let notSentMessages = {};
    if (window.localStorage.getItem('notSentMessages') != null) {
      notSentMessages = JSON.parse(window.localStorage.getItem('notSentMessages'));
      Object.keys(notSentMessages).forEach((chatId) => {
        if (chatId == this.chatId) {
          Object.keys(notSentMessages[chatId]).forEach((idMessage) => {
            if (idMessage == tempId) {
              delete notSentMessages[chatId][idMessage];
            }
          });
        }
        if (Object.keys(notSentMessages[chatId]).length == 0) {
          delete notSentMessages[chatId];
        }
      });
      window.localStorage.setItem('notSentMessages', JSON.stringify(notSentMessages));
    }
  }

  drowStatus(status, color) {
    document.getElementById('connect-status').className = `chat__status chat__status--${color}`;
    document.getElementById('connect-status').innerText = status;
    console.log(status);
  }

  soundNewMessage(message) {
    if (message.recipient == this.userId) {
      audio.play();
    }
  }

  /**
   * Обработчик события скрола чата
   */
  chatScroll() {
    this.loadMessagesHistory();
    this.searchUnreadMessage();
  }

  /**
   * Поиск непрочитанных сообщений получателя по позиции в окне чата
   */
  searchUnreadMessage() {
    setTimeout(() => {
      const areaBottomBorder = this.messageArea.getBoundingClientRect().bottom;
      const areaTopBorder = this.messageArea.getBoundingClientRect().top;
      const { unread } = this.messages;

      for (const key in unread) {
        const { element } = unread[key];

        const elementPosition = element.getBoundingClientRect().top + 15;

        if (elementPosition <= areaBottomBorder && elementPosition >= areaTopBorder) {
          this.socket.emit('messages:markAsRead', { id: element.getAttribute('data-msgid') });
          delete this.messages.unread[key];
        }
      }
    }, 500);
  }

  /**
   * Подгрузка предыдущих сообщений
   */
  loadMessagesHistory() {
    if (this.scrollHistoryMessages) {
      if (this.messageArea.scrollTop > 0 && this.messageArea.scrollTop <= 30) {
        this.getHistory();
      }
    }
  }

  /**
   * Проверка истории сообщений
   * Запрещает подгрузку истории при скролле
   * @param messagesAmount
   */
  checkHistory(messagesAmount) {
    if (messagesAmount < this.messagesLimit || messagesAmount == 0) {
      this.scrollHistoryMessages = false;
    } else {
      this.scrollHistoryMessages = true;
    }
  }

  /**
   * Возвращает временный id для отправляющегося сообщения
   * @returns {number}
   */
  tempId() {
    return this.lastSentId + 1;
  }

  /**
   * Устанавливает Id последнего отправленного сообщения
   * @param id
   */
  setLastSentId(id) {
    this.lastSentId = id;
  }

  /**
   * Принятие статуса сообщения "прочитано собеседником"
   * @param message
   */
  onReaded({ id }) {
    if (this.messages.all[id]) {
      this.messages.all[id].markAsRead();
    }
  }

  /**
   * Отправка события "я печатаю"
   */
  typing() {
    const timerTime = 3000;
    if (this.accesTypingEvent) {
      this.accesTypingEvent = false;
      this.socket.emit('messages:typing', { recipient: this.recipientId });

      setTimeout(() => {
        this.accesTypingEvent = true;
      }, timerTime);
    }
  }

  /**
   * Принятие события "собеседник печатает"
   * @param data
   */
  onCompanionTyping(data) {
    const { sender } = data;
    const timerTime = 3000;

    if (sender === this.recipientId) {
      this.countEventTyping += 1;
      this.typingBlock.innerText = 'Ваш собеседник печатает...';

      setTimeout(() => {
        if (this.countEventTyping === 1) {
          this.countEventTyping -= 1;
          this.typingBlock.innerText = '';
        }
        if (this.countEventTyping > 1) {
          this.countEventTyping -= 1;
        }
      }, timerTime);
    }
  }


  /**
   * Возвращает самый большой id сообщения из истории сообщений
   * @returns startMessageId
   */
  getStartMessageId() {
    const messagesCount = Object.keys(this.messages.all).length;
    let startMessageId = null;
    const idMessages = Object.keys(this.messages.all);
    if (messagesCount === 0) {
      startMessageId = 0;
    } else {
      startMessageId = Math.max.apply(null, idMessages);
    }
    return startMessageId;
  }

  /**
   * @returns {string}
   */
  getChatId() {
    let chatId = '';
    if (this.userId > this.recipientId) {
      chatId = `${this.recipientId}_${this.userId}`;
    } else {
      chatId = `${this.userId}_${this.recipientId}`;
    }
    return chatId;
  }
}


window.createChat = function () {
  const id = document.getElementById('user-id').value;

  window.webChat = new Client(id);
  window.webChat.connect();

  document.querySelector('.set-user-id').style.display = 'none';
  document.querySelector('.chat').style.display = 'flex';
};


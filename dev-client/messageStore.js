
export default class MessageStore {
  constructor(userId) {
    this.userId = +userId;
    this.all = {};
    this.unread = {};
    this.readyToSent = {};
    this.sent = {};
    this.delivered = {};
    this.renderMessage = {};
    this.firstRender = true;

    this.messageArea = document.getElementById('messages-area');
  }

  /**
   * Добавление сообщения в непрочитанные
   * @param message
   */
  isUnread(message) {
    if (!message.readed && this.userId != message.sender) {
      this.unread[message.id] = message;
    }
  }

  render() {
    const messagesWrapper = document.importNode(document.getElementById('message-template').content, true).children[1];

    for (const key in this.renderMessage) {
      messagesWrapper.appendChild(this.renderMessage[key].element);
    }
    if (this.firstRender) {
      this.firstRender = false;
      this.messageArea.appendChild(messagesWrapper);
      this.messageArea.scrollTop = messagesWrapper.getBoundingClientRect().bottom;
    } else {
      this.messageArea.insertBefore(messagesWrapper, this.messageArea.children[0]);
      this.messageArea.scrollTop = this.messageArea.clientHeight + (this.messageArea.scrollTop - this.messageArea.clientHeight);
    }
    this.renderMessage = {};
  }

  /**
   * Проверка на наличие отправляющегося сообщения
   * @param randomId
   * @returns {boolean}
   */
  checkSentMessage(randomId) {
    let haveSentMessage = false;
    Object.keys(this.sent).forEach((item) => {
      if (item == randomId) {
        haveSentMessage = true;
      }
    });
    return haveSentMessage;
  }

  /**
   * Проверка на наличие доставленного сообщения
   * @param id
   * @returns {boolean}
   */
  checkDeliveredMessage(id) {
    let haveSentMessage = false;
    Object.keys(this.delivered).forEach((item) => {
      if (item == id) {
        haveSentMessage = true;
      }
    });
    return haveSentMessage;
  }

  /**
   * Добавление сообщения в "отправленные"
   * @param id
   * @returns {*}
   */
  getReadyToSent(id) {
    this.sent[id] = this.readyToSent[id];
    delete this.readyToSent[id];
    return this.sent[id];
  }
}

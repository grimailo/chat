const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './dev-client/client.js',
  devServer: {
    contentBase: './dist/',
    disableHostCheck: true,
  },
  module: {
    rules: [
      {
        test: /\.mp3$/,
        loader: 'url-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      // title: 'dev client',
      // filename: __dirname + 'index.html',
      // chunks: ['index', 'common'],
      template: './dev-client/index.html',
    }),
  ],
};

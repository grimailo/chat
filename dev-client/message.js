const moment = require('moment');

export default class Message {
  constructor(userId, {
    chat_id, id, message, readed, recipient, sended_at, sender,
    delivered = true,
  }) {
    // console.log(this);
    this.userId = userId;

    this.chat_id = chat_id;
    this.id = id;
    this.message = message;
    this.readed = readed;
    this.recipient = recipient;
    this.sended_at = (sended_at == null) ? new Date() : sended_at;
    this.sender = sender;
    this.delivered = delivered;
    this.element = null;
    this.createElement();
  }

  createElement() {
    this.element = document.importNode(document.getElementById('message-template').content, true).children[0];

    this.element.querySelector('.message__sender').innerText = this.sender;
    this.element.querySelector('.message__text').innerText = this.message;
    this.element.querySelector('.message__sended').innerText = moment(this.sended_at).format('H:mm:ss D.MMM.YY');
    this.element.querySelector('.message__status-read').innerText = this.readed ? '[Прочитано]' : '[Не прочитано]';


    if (this.sender == this.userId) {
      this.element.className += ' message--own';
      this.element.querySelector('.message__status-send').innerText = this.delivered ? '[Доставлено]' : '[Отпрвавляется]';
    }

    this.element.setAttribute('data-msgid', this.id);
    this.element.setAttribute('data-sender', this.sender);
    this.element.setAttribute('data-readed', this.readed);
  }


  renderNewMessage(messageArea) {
    this.element = messageArea.appendChild(this.element);
    if (this.userId == this.sender) {
      messageArea.scrollTop = Math.ceil(messageArea.scrollHeight - messageArea.clientHeight);
    }
  }

  markAsRead() {
    this.readed = true;
    this.element.querySelector('.message__status-read').innerText = '[Прочитано]';
  }

  markAsDelivered() {
    this.delivered = true;
    this.element.querySelector('.message__status-send').innerText = '[Доставлено]';
  }

  changeId(id) {
    this.id = id;
    this.element.setAttribute('data-msgid', this.id);
  }
}

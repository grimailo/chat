const chalk = require('chalk');

const CometServer = new require('./CometServer');
const DBClient = new require('./DBClient');

const STATUS_NOT_INIT = 0;
const STATUS_WORKING = 1;
const STATUS_ERROR = 2;
const STATUS_STOPPED = 3;

class Core {
  constructor(config) {
    this.$config = config;

    this.status = STATUS_NOT_INIT;

    this.initCallbacks = [];
    this.errorCallbacks = [];
    this.quitCallbacks = [];

    this.$CometServer = new CometServer(this);
    this.$DBClient = new DBClient(this);
  }

  async init() {
    try {
      const promises = [];
      this.initCallbacks.map((callback) => {
        promises.push(callback());
      });

      await Promise.all(promises);

      this.status = STATUS_WORKING;
    } catch (err) {
      this.errorHandler(err);
    }
  }

  /**
       * Завершение работы
       * @returns {Promise<void>}
       */
  async errorHandler(error = '') {
    this.status = STATUS_ERROR;

    this.debug(`Произошла ошибка: ${error}`, 'red');

    const promises = [];

    this.errorCallbacks.map((callback) => {
      promises.push(callback());
    });

    await Promise.all(promises);

    await this.quit();
  }

  async quit() {
    if (this.status != STATUS_ERROR) this.status = STATUS_STOPPED;

    this.debug('Завершение работы');

    this.quitCallbacks.map((callback) => {
      callback();
    });
  }

  /**
       * Установить свой обработчик незапланированого завершения работы
       * @param callback
       * @returns {*}
       */
  catch(callback) {
    if (this.status == STATUS_ERROR) return callback();

    this.errorCallbacks.push(callback);
    return true;
  }

  setInitHandler(callback) {
    if (this.status == STATUS_WORKING) return callback();
    if (this.status != STATUS_NOT_INIT) return false;

    this.initCallbacks.push(callback);
    return true;
  }

  setQuitHandler(callback) {
    if (this.status == STATUS_STOPPED) return callback();

    this.quitCallbacks.push(callback);
    return true;
  }

  debug(data, color = 'cyan') {
    if (this.$config.debug) console.log(chalk[color](data));
  }
}

module.exports = Core;

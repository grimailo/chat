const http = require('http');
const socketServer = require('socket.io');
const User = require('../Models/User');

class CometServer {
  constructor(Core) {
    this.$Core = Core;

    this.config = this.$Core.$config;

    this.server = null;
    this.socket = null;

    this.$Core.setInitHandler(async () => await this.start());
    this.$Core.setQuitHandler(async () => await this.quit());
  }

  /**
       *
       * @returns {Promise<void>}
       */
  async start() {
    // Создание экзэмпляра сервера
    this.server = await this.createHttpServer();
    this.socket = socketServer(this.server, this.config.socket);

    // Проверка авторизации у каждого нового клиента
    this.socket.use((socket, next) => this.auth(socket, next));
  }

  /**
       * Создать http сервер
       * @returns {Promise<any>}
       */
  createHttpServer() {
    return new Promise((resolve, reject) => {
      const server = http.createServer()
        .on('error', (err) => {
          this.$Core.debug(
            `Не удалось создать сервер (порт ${this.config.socket.port} занят или произошла другая ошибка)`,
            'red',
          );
          reject(err);
        })
        .listen(this.config.socket.port, this.config.socket.host, () => {
          this.$Core.debug(
            `CometServer запущен по адресу //${this.config.socket.host}:${this.config.socket.port}${this.config.socket.path}`,
            'green',
          );
          resolve(server);
        });
    });
  }

  /**
      * Посредник авторизации - проверяет каждое новое соеденение,
      * проходит ли оно авторизацию если проходит создает комнату
      * @param socket
      * @param next
      * @returns {*}
      */
  auth(socket, next) {
    try {
      const { token } = socket.handshake.query;
      if (!token) throw new Error('authentication error');

      const CurrentUser = new User(token, this.config.auth.jwtSecret);
      socket.join(CurrentUser.id);
      socket.CurrentUser = CurrentUser;

      this.$Core.debug(`connect new client ${socket.id} by user ${CurrentUser.id}`, 'cyan');

      return next();
    } catch (err) {
      this.debug(err);
      socket.disconnect(true);
      return next(new Error(err));
    }
  }

  on(...args) {
    return this.socket.on(...args);
  }

  to(...args) {
    return this.socket.to(...args);
  }

  /**
       * Завершение работы
       * @returns {Promise<void>}
       */
  async quit(error) {
    this.error = true;

    // Завершение работы сокета
    if (this.socket) {
      await new Promise((resolve, reject) =>
        this.socket.close((err) => {
          if (err) reject();
          resolve();
        }));
      this.socket = null;
      this.$Core.debug('CometServer успешно завершил работу');
    }
  }
}

module.exports = CometServer;

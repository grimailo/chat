const { Pool } = require('pg');

class DBClient {
  constructor(Core) {
    this.$Core = Core;

    this.config = this.$Core.$config.db;

    this.client = null;

    this.$Core.setInitHandler(async () => await this.start());
    this.$Core.setQuitHandler(async () => await this.quit());
  }

  async start() {
    this.client = new Pool(this.config);
    const { rows: [result] } = await this.query('SELECT version();');
    this.$Core.debug(`DBClient запущен, version: ${result.version}`, 'green');
  }

  async query(...args) {
    return await this.client.query(...args);
  }

  // async insert(tableName, rows, params) {
  //   const placeholder = Array(...Array(rows.length)).map((item, value) => `$${value + 1}`);
  //   return await this.query(`INSERT INTO "${tableName}" (${rows.join(', ')}) VALUES(${placeholder})  RETURNING *`, params);
  // }

  async quit() {
    if (this.client) await this.client.end();
    this.$Core.debug('DBClient успешно запершил работу');
  }
}

module.exports = DBClient;

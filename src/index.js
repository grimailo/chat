const config = require('./config');

const core = require('./Core/Core');
const messagesController = require('./Controllers/MessagesController');

const Core = new core(config);

(async () => {
  await Core.init();

  const MessagesController = new messagesController(Core);
})();


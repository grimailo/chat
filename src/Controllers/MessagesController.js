const Messages = require('./../Models/Messages');

class MessagesController {
  constructor(Core) {
    this.$Core = Core;
    this.config = this.$Core.$config;

    this.CometServer = this.$Core.$CometServer;
    this.DBClient = this.$Core.$DBClient;

    this.Messages = new Messages(this.$Core);

    // Прослушка сокета
    this.CometServer.on('connection', s => this.listenConnection(s));
  }

  /**
   * Прослушивание соеденений клиентов на сервере
   * @param socket
   */
  listenConnection(socket) {
    socket.on('messages:getLastMessages', (data, callback) => {
      this.getLastMessages(socket, data, callback);
    });
    socket.on('messages:send', (data, callback) => {
      this.sendNewMessage(socket, data, callback);
    });
    socket.on('messages:markAsRead', (data) => {
      this.markAsReadMessage(socket, data);
    });
    socket.on('messages:typing', (data) => {
      this.typing(socket, data);
    });
  }

  /**
   * Поиск сообщений по ChatId
   * @param socket
   * @param room
   */
  async getLastMessages(socket, {
    recipient, offset, limit, startMessageId,
  }, result) {
    try {
      if (!recipient) throw new Error('recipient is not sended');
      const chat = this.chatId(socket.CurrentUser.id, recipient);

      const queryResult = await this.Messages.getLast(chat, limit, offset, startMessageId);

      result(queryResult.rows);

      this.$Core.debug(`new event: messages:getLastMessages ${chat}`, 'yellow');
    } catch (err) {
      result({ error: err.message });
      this.$Core.debug(`error in new event: messages:getLastMessages, ${err.message} `, 'red');
    }
  }


  /**
   * Запись сообщения в базу и вещание через сокет
   * @param socket
   * @param message
   */
  async sendNewMessage(socket, {
    recipient, message, tempId, sended_at,
  }, result) {
    try {
      if (!recipient) throw new Error('recipient is not sended');
      if (!message) throw new Error('message is not sended');
      if (message.length > 1024) throw new Error('message is too long');

      const sender = socket.CurrentUser.id;
      const chat = this.chatId(sender, recipient);

      const { rows: [queryResult] } = await this.Messages.add(chat, sender, recipient, message, sended_at);

      result({ tempId, messageId: queryResult.id });


      this.CometServer.to(sender).emit('messages:new', { message: queryResult });
      this.CometServer.to(recipient).emit('messages:new', { message: queryResult });

      this.$Core.debug(`new event: messages:send ${chat}, ${message}`, 'yellow');
    } catch (err) {
      result({ error: err.message });
      this.$Core.debug(`error in new event: messages:send, ${err.message} `, 'red');
    }
  }

  /**
   * Помечает сообщение в базе как прочитаное
   * Отправляет событие о прочтении сообщения отправителю
   * @param socket
   * @returns {Promise.<void>}
   */
  async markAsReadMessage(socket, { id }) {
    try {
      const { rows: [message] } = await this.Messages.get(id);

      if (socket.CurrentUser.id != message.recipient) throw new Error('you are not recipient of this message');

      await this.Messages.markAsRead(id);

      this.CometServer.to(message.sender).emit('messages:readed', { id });
      this.CometServer.to(message.recipient).emit('messages:readed', { id });
    } catch (err) {
      this.$Core.debug(err);
    }
  }

  /**
   * Обработчик события "пользователь печатает сообщение"
   * оповещает собеседника
   * @param data
   */
  typing(socket, data) {
    const sender = socket.CurrentUser.id;
    const { recipient } = data;

    this.CometServer.to(recipient).emit('messages:typing', { sender });
  }

  /**
   * Получение id чата
   * @param socket
   * @param sender
   */
  chatId(sender, recipient) {
    let chatId = '';
    sender = parseInt(sender, 10);
    recipient = parseInt(recipient, 10);
    if (sender > recipient) {
      chatId = `${recipient}_${sender}`;
    } else {
      chatId = `${sender}_${recipient}`;
    }
    return chatId;
  }
}

module.exports = MessagesController;

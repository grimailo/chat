class Messages {
  constructor(Core) {
    this.$Core = Core;

    this.DBClient = this.$Core.$DBClient;
  }

  async getLast(chat, limit, offset, lastId) {
    return await this.DBClient.query('SELECT * FROM "messages" WHERE chat_id = $1 AND CASE WHEN $2 = 0 THEN id > $2 ELSE id <= $2 END  ORDER BY id DESC LIMIT $3 OFFSET $4', [chat, lastId, limit, offset]);
  }

  async add(chat, sender, recipient, message, sended_at) {
    sender = parseInt(sender, 10);
    recipient = parseInt(recipient, 10);

    return await this.DBClient.query(
      'INSERT INTO "messages" (chat_id, sender, recipient, message, readed, sended_at) VALUES($1, $2, $3, $4, $5, $6) RETURNING *',
      [chat, sender, recipient, message, false, sended_at],
    );
  }

  async get(id) {
    return await this.DBClient.query('SELECT * FROM "messages" WHERE id = $1', [id]);
  }

  async markAsRead(id) {
    return await this.DBClient.query(
      'UPDATE "messages" SET readed = $1 WHERE id = $2',
      [true, id],
    );
  }
}

module.exports = Messages;

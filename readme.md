##Comet server

####Commands
```
# Запуск socket сервера по адресу localhost:2121
$ npm run start

# Запуск dev сервера вебпак с горячей перезагрузкой по адресу localhost:8080
$ npm run dev

# Запуск проверки кода
$ npm run lint

# Запуск проверки кода
$ npm run precommit
```

####Описание папок

__src__ - исходники сервера, точка входа - файл index.js

__dev-client__ - клиент для тестов сервера, запускается с помощью вебпака с горячей перезагрузкой. 

####Документация используемых зависмостей
1. https://socket.io/docs/ - Библиотека для создания реал-тайм сервера
2. http://redis.js.org/ - Удобный и быстрый клиент для общения с Redis сервером
3. https://github.com/chalk/chalk - Простенькая библиотека для подсветки синтаксиса

####Конфигурация сервера
Стандартные настройки находятся в файле */src/cometServer.js*.

Советую дублировать его содержимое в файл *../cometServer.js* (находящийся во внешней деректории 
относительно прокетка, для того что бы не было конфликтов в репозитории) и редактировать его.